package com.example.restservice.controller;

import com.example.restservice.model.Perro;
import com.example.restservice.repository.PerroRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.web.reactive.server.JsonPathAssertions;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class PerroControllerTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PerroRepository repository;

    @Test
    void create() {
        entityManager.persist(new Perro("firulais"));
        Perro p = repository.findByName("firulais");
        assertThat(p.getName()).isEqualTo("firulais");
    }

}