package com.example.restservice.controller;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import com.example.restservice.model.Perro;
import com.example.restservice.repository.PerroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/perro")
@RestController
public class PerroController {

	@Autowired
	private PerroRepository repo;


	@GetMapping("/greeting")
	public String greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		//return new Greeting(counter.incrementAndGet(), String.format(template, name));
        return "Holi";
	}

	@GetMapping("/")
	public List<Perro> getAll() {
		return repo.findAll();
	}

	@PostMapping("/")
	public Perro create(@RequestBody Perro p) {
		//return new Greeting(counter.incrementAndGet(), String.format(template, name));
        repo.save(p);
        return p;
	}

    @GetMapping("/ladrar")
	public String ladrar() {
		return "GUAU GUAU!!";
	}
}
