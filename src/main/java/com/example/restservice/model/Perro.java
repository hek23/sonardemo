package com.example.restservice.model;

import javax.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
public class Perro {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Integer id;

  private String name;

  public Perro(String firulais) {
    this.name=firulais;
  }
  public Perro(int id, String name){
    this.id = id;
    this.name = name;
    //this.name="notFirulais";
  }

  public Perro() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}